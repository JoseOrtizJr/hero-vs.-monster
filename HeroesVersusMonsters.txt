//Jose Ortiz Jr

import java.util.*;

public class HeroesVersusMonsters 
{

   private static Hero hero;
   private static Monster monster;
   private static Random rand = new Random();
	
   public static void main(String[] args) 
   {
      Scanner input = new Scanner(System.in);
   	
      while(true)
      {
         System.out.println("Choose the kind of Hero you wish to enter battle with:");
         System.out.println("1. The Warrior");
         System.out.println("2. The Sorceress");
         System.out.println("3. The Thief");
         System.out.println("4. The Joker");
      	
         int choice = input.nextInt();
         while(choice < 1 || choice > 4)
         {
            System.out.println("Your choice in Heroes was invalid");
            System.out.println("Choose between the 3 Heroes: ");
            choice = input.nextInt();
         }
      	
         switch(choice)
         {
            case 1: 
               hero = new Warrior();
               break;
            case 2:
               hero = new Sorceress();
               break;
            case 3:
               hero = new Thief();
               break;
            case 4:
               hero = new Joker();
               break;
         }
         switch(rand.nextInt(3))
         {
            case 0:
               monster = new Ogre("The Ogre");
               break;
            case 1:
               monster = new Gremlin("The Gremlin");
               break;
            case 2:
               monster = new Skeleton("The Skeleton");
               break;
         }
      	
         System.out.println("Your challenger is " + monster.getName());
      	
         while(hero.Life() && monster.Life())
         {
            hero.attack(monster);
            monster.attack(hero);
         }
      	
         System.out.println("Would you like to play again?");
         System.out.println("1. Yes");
         System.out.println("2. No");
      	
         int selection = input.nextInt();
      	
         while(selection < 1 || selection > 2)
         {
            System.out.println("Enter 1 if you wish to conitnue or enter 2 if you want to quit");
            selection = input.nextInt();
         }
      	
         if(selection == 2)
         {
            break;
         }
         else 
         {
            System.out.println();
         }
      }
   
   }

}
